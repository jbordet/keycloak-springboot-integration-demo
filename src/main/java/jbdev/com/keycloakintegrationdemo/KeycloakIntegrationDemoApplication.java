package jbdev.com.keycloakintegrationdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakIntegrationDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakIntegrationDemoApplication.class, args);
	}

}
